# README #

Fast Pose Solver mapping suite
A thin package for mapping with multiple depth sensors

### What is this repository for? ###

### How do I get set up? ###

The system works on ubuntu 14.04, ros indigo (but should work also on other versions)

1) install the ubuntu packages

sudo apt-get install \
     libeigen3-dev \
     libflann-dev \
     libsuitesparse-metis-dev \
     freeglut3-dev \
     libqglviewer-dev 
     

2) checkout, install and build g2o. Locally.

cd <YOUR_SRC_FOLDER_WHERE_YOU_DOWNLOAD_EXTERNAL_LIBS>
git clone https://github.com/RainerKuemmerle/g2o.git
cd g2o
mkdir build
cmake ../
make -j <as much as you have>

g2o is now compiled, we need to set up the environment so that cmake finds our local installation.


To this end add this to your ~/.bashrc
export G2O_ROOT=<YOUR_SRC_FOLDER_WHERE_YOU_DOWNLOAD_EXTERNAL_LIBS>/g2o

source ~/.bashrc

IMPORTANT!
You might need to copy the g2o/build/g2o/config.h file in g2o/g2o

  g2o$ cp build/g2o/config.h g2o

3) download this repository 

       https://bitbucket.org/ggrisetti/thin_drivers.git

   copy or link in your workspace only the following folders in thin_drivers
   
      $catkin_ws> ln -s <path to thin _drivers>/thin_msgs .
      $catkin_ws> ln -s <path to thin_drivers>/thin_state_publisher .


4) copy or link fps_mapper in your catkin workspace

      $catkin_ws> ln -s <path to fps_mapper> .

      
5) build all
  
      $catkin_ws> catkin_make

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact



### other information ###

This package comprises a bunch of tools for mapping with mobile robots
and depth cameras, but also 3D lasers

The components are rather ROS independant, and rely only on the
deb packages listed above and on g2o for global optimization


fps_globals  	    	  # global defines and functions used in the tracker
fps_core		  # core alignment routines and structures
fps_core_examples	  # simple examples of the core functionalities
fps_gl_helpers		  # opengl utilities
fps_core_viewers	  # viewers for the clouds
fps_tracker		  # sequential tracker
fps_tracker_viewers	  # sequential tracker viewers
fps_tracker_examples	  # sequential tracker examples
fps_global_optimization   # optimization package
fps_local_mapper	  # local mapper (relies on tracker)
fps_local_mapper_examples # examples on how to use the local mapper
fps_local_mapper_viewers  # viewers for the local mapper
fps_loop_closer		  # loop closer (based on trajectory)
fps_map			  # map structures (local maps and so on)
fps_map_viewers		  # map viewers
fps_ros_bridge		  # ros nodes
fps_calibration		  # calibration utilities to register multiple sensors
playground		  # misc stuff