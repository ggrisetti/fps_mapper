#pragma once

typedef int DrawAttributesType;

#define ATTRIBUTE_SHOW     0x1  // default, shows an object
#define ATTRIBUTE_SELECTED 0x2  // policy for a selected object
#define ATTRIBUTE_ONLY     0x4  // draw only attributes 

