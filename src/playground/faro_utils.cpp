#include "fps_core/spherical_projector.h"
#include "fps_core/spherical_camera_info.h"
  
namespace fps_mapper {

inline Eigen::Vector3f cartesian2polar(const Eigen::Vector3f& p){
  float range=p.norm();
  float azimuth=atan2(p.y(), p.x());
  float elevation=atan2(p.z(), sqrt(p.x()*p.x()+p.y()*p.y()));
  return Eigen::Vector3f(azimuth, elevation, range);
}

  void loadFaroFile(Vector3fVector& points, 
		    std::vector<int>& intensities,
		    std::istream& is){
    while (is){
      Eigen::Vector3f p;
      int intensity;
      is >> p.x() >> p.y() >> p.z() >> intensity;
      points.push_back(p);
      intensities.push_back(intensity);
    }
  }

  void points2sphericalImage(UnsignedShortImage& depth,
			     RGBImage& rgb,
			     float horizontal_fov,
			     float vertical_fov,
			     const Vector3fVector& points,
			     const std::vector<int>& intensities){
    depth=0;
    rgb=cv::Vec3b(0,0,0);
    int hres=depth.cols/horizontal_fov;
    int vres=depth.rows/vertical_fov;
    for (size_t i=0; i<points.size(); i++){
      Eigen::Vector3f polar_point=cartesian2polar(points[i]);
      float azimuth=polar_point(0);
      float elevation=polar_point(1);
      float range=polar_point(2);
      uint8_t intensity=intensities[i]/10;
      int col=azimuth*hres+depth.cols/2;
      int row=elevation*vres+depth.rows/2;
      if (row<0 || row>=depth.rows)
	continue;
      if (col<0 || col>=depth.cols)
	continue;
      if (range>65.535)
	continue;
      uint16_t& pixel_depth=depth.at<uint16_t>(row,col);
      cv::Vec3b& pixel_intensity=rgb.at<cv::Vec3b>(row,col);
      uint16_t new_pixel_depth=range*1000;
      if (!pixel_depth || new_pixel_depth<pixel_depth){
	pixel_depth=new_pixel_depth;
	pixel_intensity=cv::Vec3b(intensity, intensity, intensity);
      }
    }
  }  
}
