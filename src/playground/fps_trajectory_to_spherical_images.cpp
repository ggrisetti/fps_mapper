#include <fstream>
#include <stdexcept>
#include "globals/system_utils.h"
#include "boss/deserializer.h"
#include "boss/trusted_loaders.h"
#include "fps_map/image_map_node.h"
#include "fps_map/local_map.h"
#include "fps_core/spherical_projector.h"
#include "fps_core/spherical_camera_info.h"
#include "txt_io/message_writer.h"
#include "txt_io/spherical_image_message.h"
#include "fps_core/depth_utils.h"

using namespace fps_mapper;
using namespace boss;
using namespace std;


BaseCameraInfo cinfo;
ImageMapNode tnode;
LocalMap lmap;
BinaryNodeRelation rel;

const char* banner[]= {
  "have to write the banner",
  0
};

int main (int argc, char** argv) {
  if (argc<2 || ! strcmp(argv[1],"-h")) {
    system_utils::printBanner(banner);
    return 0 ;
  }

  int cols=360*4;
  int rows=90*4;
  float horizontal_fov=2*M_PI;
  float vertical_fov=M_PI/2;

  std::list<Serializable*> objects;
  Deserializer des;
  des.setFilePath(argv[1]);
  Serializable* o;
  SphericalProjector projector;
  projector.setImageSize(rows,cols);
  SphericalCameraInfo camera_info;
 
  
  Eigen::Vector4f camera_matrix;
  camera_matrix << horizontal_fov, vertical_fov, cols/horizontal_fov, rows/vertical_fov;
  camera_info.setCameraMatrix(camera_matrix);
  camera_info.setTopic("spherical_image");
  camera_info.setFrameId("spherical_image_frame_id");
  projector.setCameraInfo(&camera_info);
  txt_io::MessageWriter message_writer;
  message_writer.open(argv[2]);

  while ( (o = des.readObject()) ){
    objects.push_back(o);
    LocalMap* local_map=dynamic_cast<LocalMap*>(o);
    if (local_map) {
      cerr << "Convrting: " << local_map->getId() << endl;
      txt_io::SphericalImageMessage image_message("spherical_image", 
						  "spherical_image_frame_id", 
						  local_map->getId());
      image_message.setCameraMatrix(camera_matrix);
       FloatImage depth_image;
       depth_image.create(rows,cols);
       IntImage index_image;
       index_image.create(rows,cols);
       projector.project(depth_image,index_image, Eigen::Isometry3f::Identity(), *local_map->cloudReference().get());
       image_message.setOdometry(local_map->transform());
       UnsignedShortImage depth_raw;
       convert_32FC1_to_16UC1(depth_raw,depth_image);
       image_message.setImage(depth_raw);
       message_writer.writeMessage(image_message);
    }
  }
  cerr << "Read: " << objects.size() << " elements" << endl;
  return 1;
}
