#pragma once

#include "trajectory_energy_calculator.h"

namespace fps_mapper {

  /* The TrajectoryEnergyMorphingCalculator class compute the energy necessary to morph the trajectory of
     one local map over an other */
  class TrajectoryEnergyMorphingCalculator: public TrajectoryEnergyCalculator {
  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;

    /* Constructors and destructor */
    TrajectoryEnergyMorphingCalculator();

    /* Get methods */
    inline bool rigidMorphing() const { return _rigid_morphig; }
    inline float poseRatioThreshold() const { return _pose_ratio_threshold; }

    /* Set methods */
    inline void setRigidMorphing(const bool rigid_morphing_) { _rigid_morphig = rigid_morphing_; }
    inline void setPoseRatioThreshold(const float pose_ratio_threshold_) { _pose_ratio_threshold = pose_ratio_threshold_; }

    /* Compute morphing energy */
    virtual float compute(const LocalMap* target_local_map, const LocalMap* source_local_map, 
			  Eigen::Isometry3f source_offset = Eigen::Isometry3f::Identity()) const;
  
  protected:
    /* Compute the nearest node with respect to a set of nodes */
    void _find_nearest_node(std::set<const MapNode*>& nearest_nodes,
			    float& min_distance, Eigen::Isometry3f& delta_transform,
			    const MapNodeList& target_nodes, 
			    const std::tr1::shared_ptr<MapNode>& source_node, 
			    const Eigen::Isometry3f source_offset = Eigen::Isometry3f::Identity(),
			    Eigen::Isometry3f update = Eigen::Isometry3f::Identity()) const;

    /* If true, each time a node of the trajectory is morphed, also the successive nodes are moved
       accordingly */
    bool _rigid_morphig;
    /* minimum ratio between the number of poses in the input local maps for which the comparison is
       considered not good */
    float _pose_ratio_threshold;
  };
    
}
