#include "trajectory_aligner_solver.h"

namespace fps_mapper {

  TrajectoryAlignerSolver::TrajectoryAlignerSolver() {
    _error = 0;
    _damping = 100;
    _transform.setIdentity();
    _b.setZero();
    _H.setZero();
    _target = 0;
    _source = 0;
  }

  void TrajectoryAlignerSolver::computeErrorAndJacobian(Eigen::Vector3f& point_error, Eigen::Vector3f& normal_error,
							Matrix6f& J, 
							const Eigen::Vector3f& target_point, const Eigen::Vector3f& target_normal,
							const Eigen::Vector3f& source_point, const Eigen::Vector3f& source_normal) const {
    J.setZero();
    Eigen::Vector3f transformed_source_point = _transform * source_point;
    Eigen::Vector3f transformed_source_normal = _transform.linear() * source_normal;
    point_error = target_point - transformed_source_point;
    normal_error = target_normal - transformed_source_normal;
    J.block<3, 3>(0, 0).setIdentity();  
    J.block<3, 3>(0, 3) = -2.0f * skew(transformed_source_point);
    J.block<3, 3>(3, 3) = -2.0f * skew(transformed_source_normal);
  }

  void TrajectoryAlignerSolver::linearize(const TrajectoryAlignerCorrespondenceFinder::CorrespondenceVector& correspondences) {
    _error = 0;
    _H.setZero();
    _b.setZero();

    const Eigen::Vector3f z(0.0f, 0.0f, 1.0f);
    for(size_t i = 0; i < correspondences.size(); ++i) {
      Eigen::Vector3f point_error;
      Eigen::Vector3f normal_error;
      std::tr1::shared_ptr<MapNode> source_trajectory_node = correspondences[i].second;
      Eigen::Isometry3f source_trajectory_node_transform = source_trajectory_node->transform();
      std::tr1::shared_ptr<MapNode> target_trajectory_node = correspondences[i].first;
      Eigen::Isometry3f target_trajectory_node_transform = target_trajectory_node->transform();
      const Eigen::Vector3f& source_point = source_trajectory_node_transform.translation();
      const Eigen::Vector3f source_normal = source_trajectory_node_transform.linear() * z;
      const Eigen::Vector3f& target_point = target_trajectory_node_transform.translation();
      const Eigen::Vector3f target_normal = target_trajectory_node_transform.linear() * z;

      Matrix6f J;
      Matrix6f Omega = 1000.0f * Matrix6f::Identity();
      computeErrorAndJacobian(point_error, normal_error, J, 
			      target_point, target_normal, 
			      source_point, source_normal);
      Vector6f error;
      error.block<3, 1>(0, 0) = point_error;
      error.block<3, 1>(3, 0) = normal_error;
      _H.noalias() += J.transpose() * Omega * J;
      _b.noalias() += J.transpose() * Omega * error;    
      _error += error.transpose() * Omega * error;      
    }           
  }

  void TrajectoryAlignerSolver::solve(const TrajectoryAlignerCorrespondenceFinder::CorrespondenceVector& correspondences) {
    linearize(correspondences);

    Vector6f b = _b;
    Matrix6f H = _H;
    H += _damping * Matrix6f::Identity();

    Vector6f delta_transform = H.ldlt().solve(b);
    _transform = v2t(delta_transform) * _transform;
    Eigen::Matrix3f R = _transform.linear();
    Eigen::Matrix3f E = R.transpose() * R;
    E.diagonal().array() -= 1;
    _transform.linear() -= 0.5 * R * E;    
  }

}
