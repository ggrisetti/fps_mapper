#include "trajectory_aligner.h"

namespace fps_mapper {
  
  TrajectoryAligner::TrajectoryAligner(): _correspondence_finder(&_solver) {
    _iterations = 10;
    _convergence_error = 1e-5;
    _correspondence_finder.setMaxPointDistance(1.0f);    
    _solver.setDamping(100);    
  }

  void TrajectoryAligner::align(const Eigen::Isometry3f& initial_guess) {
    if(!_solver.targetLocalMap()) {  
      throw std::runtime_error("[ERROR]: TrajectoryAligner's target local map not set"); 
    }
    if(!_solver.sourceLocalMap()) {
      throw std::runtime_error("[ERROR]: TrajectoryAligner's source local map not set");
    }

    _solver.setTransform(initial_guess);
    for(int i = 0; i < _iterations; ++i) {
      _correspondence_finder.findCorrespondences();
      const TrajectoryAlignerCorrespondenceFinder::CorrespondenceVector& correspondences = _correspondence_finder.correspondences();
      _solver.solve(correspondences);
      if(_solver.error() < _convergence_error) { break; }
    }
  }

}
