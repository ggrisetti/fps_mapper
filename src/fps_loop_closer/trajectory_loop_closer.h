#pragma once

#include "fps_core/multi_projector.h"

#include "base_loop_closer.h"
#include "trajectory_aligner.h"
#include "trajectory_energy_calculator.h"

namespace fps_mapper {

  /* The TrajectoryLoopCloser class performs trajectory based loop closure search */
  class TrajectoryLoopCloser: public BaseLoopCloser {
  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW    

    /* Constructors and destructor */
    TrajectoryLoopCloser(TrajectoryAligner* trajectory_aligner_, 
			 TrajectoryEnergyCalculator* trajecotry_energy_calculator_, 
			 BaseAligner* aligner_); 
    virtual ~TrajectoryLoopCloser();

    /* Get methods */
    inline int maxNeighbors() const { return _max_neighbors; }
    inline int trajecotryAlignerRefiningSteps() const { return _trajectory_aligner_refining_steps; }
    inline float maxEnergy() const { return _max_energy; }
    inline float maxDistance() const { return _max_distance; }
    inline float maxEnergyCandidateRatio() const { return _max_energy_candidate_ratio; }
    inline float straightnessMinLogRatio() const { return _straightness_min_log_ratio; }
    inline float maxBadPointDistance() const { return _max_bad_point_distance; }
    inline float maxBadPointRatio() const { return _max_bad_point_ratio; }
    inline float maxProjectorDistance() const { return _max_projector_distance; }
    inline TrajectoryAligner* trajectoryMatcher() { return _trajectory_aligner; }
    inline TrajectoryEnergyCalculator* trajectoryEnergyCalculator() { return _trajectory_energy_calculator; }

    /* Set methods */
    inline void setMaxNeighbors(const int max_neighbors_) { _max_neighbors = max_neighbors_; }
    inline void setTrajecotryAlignerRefiningSteps(const int trajectory_aligner_refining_steps_) { _trajectory_aligner_refining_steps = trajectory_aligner_refining_steps_; }
    inline void setMaxEnergy(const float max_energy_) { _max_energy = max_energy_; }
    inline void setMaxDistance(const float max_distance_) { _max_distance = max_distance_; }
    inline void setMaxEnergyCandidateRatio(const float max_energy_candidate_ratio_) { _max_energy_candidate_ratio = max_energy_candidate_ratio_; }
    inline void setStraightnessMinLogRatio(const float straightness_min_log_ratio_) { _straightness_min_log_ratio = straightness_min_log_ratio_; }
    inline void setMaxBadPointDistance(const float max_bad_point_distance_) { _max_bad_point_distance = max_bad_point_distance_; }
    inline void setMaxBadPointRatio(const float max_bad_point_ratio_) { _max_bad_point_ratio = max_bad_point_ratio_; }
    inline void setMaxProjectorDistance(const float max_projector_distance_) { 
      _max_projector_distance = max_projector_distance_; 
      if(_projector) { _projector->setMaxDistance(_max_projector_distance); }
    }
    inline void setTrajectoryAligner(TrajectoryAligner* trajectory_aligner_) { _trajectory_aligner = trajectory_aligner_; }
    inline void setTrajectoryEnergyCalculator(TrajectoryEnergyCalculator* trajectory_energy_calculator_) { _trajectory_energy_calculator = trajectory_energy_calculator_; }
 
    /* Find closures for the input local map */
    virtual void findClosures(LocalMap* target_local_map, MapNodeList* source_local_maps);

    /* Register two input local map trajectories */
    Eigen::Isometry3f align_trajectories(LocalMap* target_local_map, LocalMap* source_local_map) const;
    /* Aligns and performs geometry check on two input local maps */
    void alignAndVerifyConsistency(BinaryNodeRelation*& closure_relation,
				    LocalMap* target_local_map, LocalMap* surce_local_map, 
				   Eigen::Isometry3f initial_guess, bool energy_based);
    void computeConsistency(const FloatImage& target_depth, const IntImage& target_indices,
			     const FloatImage& source_depth, const IntImage& source_indices);
    /* Set the internal MultiProjector */
    void setProjector(const LocalMap* local_map);
    /* Check if a trajectory is straight */
    bool is_straight_trajectory(LocalMap* local_map);

  protected:   
    int _max_neighbors, _trajectory_aligner_refining_steps;
    float _max_energy, _max_energy_candidate_ratio, _straightness_min_log_ratio; 
    float _max_distance, _max_bad_point_distance, _max_bad_point_ratio, _max_projector_distance;
    BaseProjector* _projector;
    TrajectoryAligner* _trajectory_aligner;
    TrajectoryEnergyCalculator* _trajectory_energy_calculator;

    float _in_distance, _out_distance;
    int _in_num, _out_num;
    FloatImage _source_depth, _target_depth;
    IndexImage _source_indices, _target_indices;
  };

}
