#pragma once

#include "fps_map/local_map.h"

#include "trajectory_aligner_correspondence_finder.h"

namespace fps_mapper {

  /* The TrajectoryAlignerSolver solve a linearized system around the current best
     aligning transform between two local map trajectories */
  class TrajectoryAlignerSolver {
  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
    
    /* Float matrix 3x6*/
    typedef Eigen::Matrix<float, 3, 6> Matrix3_6f;

    /* Constructors and destructor */
    TrajectoryAlignerSolver();
    virtual ~TrajectoryAlignerSolver() {}

    /* Get methods */
    inline float error() const { return _error; }
    inline float damping() const { return _damping; }
    inline const Eigen::Isometry3f& transform() const { return _transform; }    
    inline const Vector6f& b() const { return _b; } 
    inline const Matrix6f& H() const { return _H; }
    inline const LocalMap* targetLocalMap() const { return _target; }
    inline const LocalMap* sourceLocalMap() const { return _source; }

    /* Set methods */
    inline void setDamping(const float damping_) { _damping = damping_; }
    inline void setTransform(const Eigen::Isometry3f& transform_) { _transform = transform_; }
    inline void setTargetLocalMap(LocalMap* local_map) { _target = local_map; }
    inline void setSourceLocalMap(LocalMap* local_map) { _source = local_map; }
    
    /* Compute error and jacobian */
    void computeErrorAndJacobian(Eigen::Vector3f& point_error, Eigen::Vector3f& normal_error, 
				 Matrix6f& J, 
				 const Eigen::Vector3f& target_point, const Eigen::Vector3f& target_normal,
				 const Eigen::Vector3f& source_point, const Eigen::Vector3f& source_normal) const;
    /* Linearize the system */
    virtual void linearize(const TrajectoryAlignerCorrespondenceFinder::CorrespondenceVector& correspondences);
    /* Compute the solution after system linearization */
    virtual void solve(const TrajectoryAlignerCorrespondenceFinder::CorrespondenceVector& correspondences);

  protected:
    /* Linearization error and dumping factor */
    float _error, _damping;
    /* Output transform */
    Eigen::Isometry3f _transform;
    /* Constant terms vector of the linear system */
    Vector6f _b;
    /* Coefficient matrix of the linear system */
    Matrix6f _H;
    /* Target local map */
    LocalMap* _target;
    /* Source local map */
    LocalMap* _source;

 };

}
