#pragma once

#include "trajectory_aligner_correspondence_finder.h"
#include "trajectory_aligner_solver.h"

namespace fps_mapper {
  
  /* The TrajectoryAligner class register two input local map trajectories */
  class TrajectoryAligner {
  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;

    /* Constructors and destructor */
    TrajectoryAligner();
    virtual ~TrajectoryAligner() {}

    /* Get methods */
    inline int iterations() const { return _iterations; }
    inline float convergenceError() const { return _convergence_error; }
    inline const LocalMap* sourceLocalMap() const { return _solver.sourceLocalMap(); }
    inline const LocalMap* targetLocalMap() const { return _solver.targetLocalMap(); }
    inline TrajectoryAlignerCorrespondenceFinder& correspondeceFinder() { return _correspondence_finder; }
    inline TrajectoryAlignerSolver& solver() { return _solver; }    
    inline const Eigen::Isometry3f& transform() const { return _solver.transform(); }
   
    /* Set methods */
    inline void setIterations(const int iterations_) { _iterations = iterations_; }
    inline void setConvergenceError(const float convergence_error_) { _convergence_error = convergence_error_; }
    inline void setSourceLocalMap(LocalMap* local_map) { _solver.setSourceLocalMap(local_map); }
    inline void setTargetLocalMap(LocalMap* local_map) { _solver.setTargetLocalMap(local_map); }

    /* Align target and source trajectories */
    virtual void align(const Eigen::Isometry3f& initial_guess = Eigen::Isometry3f::Identity());

  protected:
    /* Alignment iterations */
    int _iterations;
    /* Convergence error value */
    float _convergence_error;
    /* Correspondence finder */
    TrajectoryAlignerCorrespondenceFinder _correspondence_finder;
    /* Linearizer */
    TrajectoryAlignerSolver _solver;

  };
}
