#include <iostream>

#include <qapplication.h>
#include <qevent.h>

#include "boss/deserializer.h"
#include "fps_core/nn_aligner.h"
#include "fps_map/local_map.h"
#include "fps_map_viewers/trajectory_viewer.h"
#include "fps_global_optimization/g2o_bridge.h"
#include "fps_gl_helpers/opengl_primitives.h"

#include "trajectory_loop_closer.h"
#include "trajectory_energy_morphing_calculator.h"

using namespace GLHelpers;
using namespace boss;
using namespace fps_mapper;

class LoopCloser2TestFunctiosViewer: public TrajectoryViewer {
public:
  LoopCloser2TestFunctiosViewer(TrajectoryLoopCloser* trajectory_loop_closer_): TrajectoryViewer() {
    _need_redraw = true;
    _rigid_morphig = false;
    _max_energy = 0.06f;
    _min_log_ratio = 0.01f;
    _pose_ratio_threshold = 0.85f;
    _last_alignment_relation = 0;
    _target_local_map = 0;
    _source_local_map = 0;
    _trajectory_loop_closer = trajectory_loop_closer_;
    _trajectory_loop_closer->setMaxEnergy(_max_energy);
  }  
  virtual ~LoopCloser2TestFunctiosViewer() {}
  
  void init() {
    TrajectoryViewer::init();
    setAxisIsDrawn(true);
    setBackgroundColor(QColor(255, 255, 255));
    _need_redraw = true;
  }
  
  void optimizeMap() {
    _g2o_bridge.psToG2o(relations, nodes);
    _g2o_bridge.quietOptimize(25);
    _g2o_bridge.g2oToPs(nodes);
  }

  bool check_double_selection() {
    if(_selected_objects.size() != 2) {
      std::cout << "[WARNING]: you must select exactly two local maps... I do nothing" << std::endl;	
      return false;
    }
    std::set<MapNode*>::iterator it = _selected_objects.begin();
    _target_local_map = dynamic_cast<LocalMap*>(*it);
    _source_local_map = dynamic_cast<LocalMap*>(*++it);
    if(!_target_local_map || !_source_local_map) {
      std::cout << "[WARNING]: you must select exactly two local maps... I do nothing" << std::endl;	
      _target_local_map = 0;
      _source_local_map = 0;
      return false;
    }
    return true;
  }

  bool check_single_selection() {
    if(_selected_objects.size() != 1) {
      std::cout << "[WARNING]: you must select exactly one local maps... I do nothing" << std::endl;	
      return false;
    }
    std::set<MapNode*>::iterator it = _selected_objects.begin();
    _source_local_map = 0;
    _target_local_map = dynamic_cast<LocalMap*>(*it);
    if(!_target_local_map) {
      std::cout << "[WARNING]: you must select exactly one local maps... I do nothing" << std::endl;	
      _target_local_map = 0;
      return false;
    }
    return true;
  }

  bool is_straight_trajectory(LocalMap* local_map) {
    float x = 0.0f, xx = 0.0f;
    float y = 0.0f, yy = 0.0f;
    float z = 0.0f, zz = 0.0f;
    float vx = 0.0f, vy = 0.0f, vz = 0.0f;    
    for(MapNodeList::iterator it = local_map->nodes().begin(); it != local_map->nodes().end(); ++it) {
      x  += (*it)->transform().translation().x();
      xx += (*it)->transform().translation().x() * (*it)->transform().translation().x(); 
      y  += (*it)->transform().translation().y();
      yy += (*it)->transform().translation().y() * (*it)->transform().translation().y(); 
      z  += (*it)->transform().translation().z();
      zz += (*it)->transform().translation().z() * (*it)->transform().translation().z(); 
    }      
    float n_inv = 1.0f / (float)local_map->nodes().size();    
    vx = n_inv * (xx - n_inv * x * x); 
    vy = n_inv * (yy - n_inv * y * y); 
    vz = n_inv * (zz - n_inv * z * z); 

    std::cout << "[INFO]: variance " << vx << ", " << vy << ", " << vz << std::endl;
    if(vx > _min_log_ratio && vy > _min_log_ratio) { 
      std::cout << "[INFO]: NOT a straight trajectory" << std::endl;
      return false; 
    }
    std::cout << "[INFO]: straight trajectory" << std::endl;
    return true;
  }

  virtual void keyPressEvent(QKeyEvent* e) {
    // Reset everything [R]
    if(e->key() == Qt::Key_R) { 
      std::cout << "[INFO]: reset" << std::endl;
      _target_local_map = 0;
      _source_local_map = 0;
      _energy_candidates.clear();
      _selected_objects.clear();
      _candidate_closure_nodes.clear();
      _accepted_closure_nodes.clear();       
    }
    // Clear all visualized objects [C]
    else if(e->key() == Qt::Key_C) { 
      std::cout << "[INFO]: clear all objects" << std::endl;
      _selected_objects.clear(); 
    }
    // Visualize all local maps [V]
    else if(e->key() == Qt::Key_V) { 
      std::cout << "[INFO]: show all local maps" << std::endl;
      for(MapNodeList::iterator it = nodes.begin(); it!=nodes.end(); it++) { _selected_objects.insert(it->get()); } 
    }    
    // Undo the last source local map transform change [Z]  
    if(e->key() == Qt::Key_Z) { 
      std::cout << "[INFO]: undo the last source local map transform change" << std::endl;
      if(_source_local_map) { _source_local_map->pop(); }
    }
    // Optimize the map graph [O]
    else if(e->key() == Qt::Key_O) { 
      std::cout << "[INFO]: graph optimization" << std::endl;
      optimizeMap();
    }
    // Align two local maps [A]
    else if(e->key() == Qt::Key_A) { 
      std::cout << "[INFO]: local map alignment" << std::endl;
      if(!check_double_selection()) { return; }
      Eigen::Isometry3f initial_guess = Eigen::Isometry3f::Identity();
      initial_guess = _target_local_map->transform().inverse() * _source_local_map->transform();
      if(_last_alignment_relation) { delete _last_alignment_relation; }
      _last_alignment_relation = _trajectory_loop_closer->matchLocalMaps(_target_local_map, _source_local_map, initial_guess, 0.05f);
      _source_local_map->push();
      _source_local_map->setTransform(_target_local_map->transform() * _last_alignment_relation->transform()); 
      std::cout << "[INFO] transform " << t2v(_last_alignment_relation->transform()).transpose() << std::endl;
    }
    // Add the last edge computed [I]
    else if(e->key() == Qt::Key_I) {
      std::cout << "[INFO]: accept edge" << std::endl;
      if(_last_alignment_relation) {
	relations.insert(std::tr1::shared_ptr<BinaryNodeRelation>(_last_alignment_relation));
	_last_alignment_relation = 0;
      }
    }
    // Align trajectories [T]
    else if(e->key() == Qt::Key_T) {
      std::cout << "[INFO]: trajectory local map alignment" << std::endl;
      if(!check_double_selection()) { return; }
      Eigen::Isometry3f transform = Eigen::Isometry3f::Identity();
      transform = _trajectory_loop_closer->align_trajectories(_target_local_map, _source_local_map);
      _source_local_map->push();
      _source_local_map->setTransform(_target_local_map->transform() * transform);
    }
    // Align trajectory, cloud and then perform geometry check [P]
    else if(e->key() == Qt::Key_P) {
      std::cout << "[INFO]: loop closing" << std::endl;
      if(!check_single_selection()) { return; }
      
      _trajectory_loop_closer->findClosures(_target_local_map, &nodes);      
      BinaryNodeRelationSet& relations = _trajectory_loop_closer->acceptedClosureRelations();      
      _candidate_closure_nodes = _trajectory_loop_closer->candidateClosureNodes(); 
      _accepted_closure_nodes = _trajectory_loop_closer->acceptedClosureNodes();
    }
    // Geometry check [G]
    else if(e->key() == Qt::Key_G) {
      std::cout << "[INFO]: geometry check" << std::endl;
      if(!check_double_selection()) { return; }
      Eigen::Isometry3f initial_guess = Eigen::Isometry3f::Identity();
      initial_guess = _target_local_map->transform().inverse() * _source_local_map->transform();
      if(_last_alignment_relation) { delete _last_alignment_relation; }      
      _trajectory_loop_closer->alignAndVerifyConsistency(_last_alignment_relation, _target_local_map, 
							 _source_local_map, initial_guess, true);
      if(_last_alignment_relation) { 
	std::cout << "[INFO] transform " << t2v(_last_alignment_relation->transform()).transpose() << std::endl;
      	_source_local_map->push();
      	_source_local_map->setTransform(_target_local_map->transform() * _last_alignment_relation->transform()); 
      }
    }
    // compute energy [E]
    else if(e->key() == Qt::Key_E) {
      std::cout << "[INFO]: compute pair energy" << std::endl;
      if(!check_double_selection()) { return; }
      Eigen::Isometry3f initial_guess = _trajectory_loop_closer->align_trajectories(_target_local_map, 
										    _source_local_map); 
      float energy = _trajectory_loop_closer->trajectoryEnergyCalculator()->compute(_target_local_map, 
										    _source_local_map, 
										    initial_guess);
      std::cout << "[INFO]: energy " << energy << std::endl;
    }
    // compute variation [F]
    else if(e->key() == Qt::Key_F) {
      std::cout << "[INFO]: compute trajectory variation " << std::endl;
      if(!check_single_selection()) { return; }            
      is_straight_trajectory(_target_local_map);
    }
    // compute straightness an all local maps [S]
    else if(e->key() == Qt::Key_S) {
      std::cout << "[INFO]: compute trajectory variation on all local maps " << std::endl;
      _energy_candidates.clear();
      for(MapNodeList::const_iterator it = nodes.begin(); it != nodes.end(); ++it) {
	LocalMap* local_map = dynamic_cast<LocalMap*>(it->get());	
	if(!local_map) { continue; }
	bool straight = _trajectory_loop_closer->is_straight_trajectory(local_map);
	if(!straight) { _energy_candidates.push_back(local_map); }
      }
    }
    // compute energy [L]
    else if(e->key() == Qt::Key_L) {
      std::cout << "[INFO]: compute energy based candidates" << std::endl;
      int counter = 0;
      _energy_candidates.clear();
      if(!check_single_selection()) { return; }
      if(is_straight_trajectory(_target_local_map)) { return; }
      for(MapNodeList::const_iterator it = nodes.begin(); it != nodes.end(); ++it) {
	LocalMap* source_local_map = dynamic_cast<LocalMap*>(it->get());
	
	if(is_straight_trajectory(source_local_map)) { continue; }
	if(!source_local_map) { continue; }
	if(!_target_local_map) { continue; }
	if(source_local_map == _target_local_map) { continue; }      
	
	Eigen::Isometry3f initial_guess = _trajectory_loop_closer->align_trajectories(_target_local_map, 
										      source_local_map); 
	float energy = _trajectory_loop_closer->trajectoryEnergyCalculator()->compute(_target_local_map, 
										      source_local_map, 
										      initial_guess);
	if(energy > 0 && energy < _max_energy) { _energy_candidates.push_back(source_local_map); }
	counter++;
      }
      if(_energy_candidates.size() && counter) {
	std::cout << "[INFO]: ratio " << (float)_energy_candidates.size() / (float)counter << std::endl;
      } 
    }
    else if(e->key() == Qt::Key_J) {
      TrajectoryEnergyMorphingCalculator* t = dynamic_cast<TrajectoryEnergyMorphingCalculator*>(_trajectory_loop_closer->trajectoryEnergyCalculator());
      if(!t) { return; }
      if(_rigid_morphig) {
	std::cout << "[INFO]: rigid morphing disabled" << std::endl;
	_rigid_morphig = false;
      }
      else {
	std::cout << "[INFO]: rigid morphing enabled" << std::endl;
	_rigid_morphig = true;
      }
      t->setRigidMorphing(_rigid_morphig);
      std::cout << "[INFO]: max energy " << _max_energy << std::endl;
    }
    else if((e->key() == Qt::Key_Plus) && (e->modifiers().testFlag(Qt::AltModifier))) {
      _min_log_ratio += 0.005;
      std::cout << "[INFO]: min log ratio " << _min_log_ratio << std::endl;      

    }
    else if((e->key() == Qt::Key_Minus)  && (e->modifiers().testFlag(Qt::AltModifier))) {
      _min_log_ratio -= 0.005;
      if(_min_log_ratio <= 0.0f) { _min_log_ratio = 0.005f; }
      std::cout << "[INFO]: min log ratio " << _min_log_ratio << std::endl;      
      _trajectory_loop_closer->setStraightnessMinLogRatio(_min_log_ratio);
    }
    else if((e->key() == Qt::Key_Plus) && (e->modifiers().testFlag(Qt::ControlModifier))) {
      TrajectoryEnergyMorphingCalculator* ec = dynamic_cast<TrajectoryEnergyMorphingCalculator*>(_trajectory_loop_closer->trajectoryEnergyCalculator());
      if(ec) {
	_pose_ratio_threshold += 0.005;
	std::cout << "[INFO]: pose ratio threshold " << _pose_ratio_threshold << std::endl;      
      	ec->setPoseRatioThreshold(_pose_ratio_threshold);      
      }
      else { std::cout << "[WARNING]: Not an energy moprhing calculator " << std::endl; }
    }
    else if((e->key() == Qt::Key_Minus)  && (e->modifiers().testFlag(Qt::ControlModifier))) {
      TrajectoryEnergyMorphingCalculator* ec = dynamic_cast<TrajectoryEnergyMorphingCalculator*>(_trajectory_loop_closer->trajectoryEnergyCalculator());
      if(ec) {
	_pose_ratio_threshold -= 0.005;
	if(_pose_ratio_threshold <= 0.0f) { _pose_ratio_threshold = 0.005f; }	
	std::cout << "[INFO]: pose ratio threshold " << _pose_ratio_threshold << std::endl;      
      	ec->setPoseRatioThreshold(_pose_ratio_threshold);      
      }
      else { std::cout << "[WARNING]: Not an energy moprhing calculator " << std::endl; }
    }
    else if(e->key() == Qt::Key_Plus) {
      _max_energy += 0.005f;
      _trajectory_loop_closer->setMaxEnergy(_max_energy);
      std::cout << "[INFO]: max energy " << _max_energy << std::endl;
    }
    else if(e->key() == Qt::Key_Minus) {
      _max_energy -= 0.005f;
      if(_max_energy < 0.0f) { _max_energy = 0.0f; }      
      _trajectory_loop_closer->setMaxEnergy(_max_energy);
      std::cout << "[INFO]: max energy " << _max_energy << std::endl;
    }
    else { 
      TrajectoryViewer::keyPressEvent(e); 
    }
    _need_redraw = true;
  }
  
  virtual void draw() {
    _need_redraw = false;

    for(std::list<MapNode*>::const_iterator it = _energy_candidates.begin(); it != _energy_candidates.end(); ++it) {
      int attrs = ATTRIBUTE_SHOW;    
      attrs |= ATTRIBUTE_SELECTED;
      attrs |= ATTRIBUTE_ONLY;
      MapNode* n = *it;      
      n->draw(attrs);
    }

    TrajectoryViewer::draw();

    for(MapNodeSet::iterator it = _accepted_closure_nodes.begin(); it != _accepted_closure_nodes.end(); ++it) {
      int attrs = ATTRIBUTE_SHOW;    
      attrs |= ATTRIBUTE_SELECTED;
      glColor3f(0.0f, 1.0f, 0.0f);
      (*it)->draw(attrs);
    }
    for(MapNodeSet::iterator it = _candidate_closure_nodes.begin(); it != _candidate_closure_nodes.end(); ++it) {
      int attrs = ATTRIBUTE_SHOW;    
      attrs |= ATTRIBUTE_SELECTED;
      glColor3f(1.0f, 0.0f, 0.0f);
      (*it)->draw(attrs);
    }
  }
  
  bool needRedraw() const { return _need_redraw; }
  const TrajectoryLoopCloser* trajectoryLoopCloser() const { return _trajectory_loop_closer; }

  void setNeedRedraw(const bool need_redraw_) { _need_redraw = need_redraw_; }
  void setTrajectoryLoopCloser(TrajectoryLoopCloser* trajectory_loop_closer_) { _trajectory_loop_closer = trajectory_loop_closer_; }

protected:
  bool _need_redraw;
  bool _rigid_morphig;
  float _max_energy, _min_log_ratio, _pose_ratio_threshold;
  BinaryNodeRelation* _last_alignment_relation;
  LocalMap* _target_local_map; 
  LocalMap* _source_local_map;
  G2OBridge _g2o_bridge;
  MapNodeSet _candidate_closure_nodes;
  MapNodeSet _accepted_closure_nodes;
  std::list<MapNode*> _energy_candidates;
  TrajectoryLoopCloser* _trajectory_loop_closer;

};

int main(int argc, char** argv) {
  if(argc < 2) {
    std::cout << "[INFO]: usage -> fps_loop_closer2_test_functions <input_log>" << std::endl;
    return 0;
  }

  NNAligner aligner;
  aligner.finder().setPointsDistance(1.0f);
  aligner.finder().setNormalAngle(M_PI / 4.0f);
  aligner.setIterations(30);
  TrajectoryAligner trajectory_aligner;
  TrajectoryEnergyMorphingCalculator trajectory_energy_calculator;
  TrajectoryLoopCloser trajectory_loop_closer(&trajectory_aligner, &trajectory_energy_calculator, &aligner);
  trajectory_loop_closer.setVerbose(true);
  trajectory_loop_closer.setMaxBadPointRatio(0.3f);
  trajectory_loop_closer.setMaxBadPointDistance(0.075f);

  QApplication app(argc, argv);
  LoopCloser2TestFunctiosViewer viewer(&trajectory_loop_closer);
  std::list<Serializable*> objects;
  Deserializer des;
  des.setFilePath(argv[1]);
  Serializable* o;  
  while((o = des.readObject())) {
    LocalMap* lmap = dynamic_cast<LocalMap*>(o);
    if(lmap) { viewer.nodes.addElement(lmap); }
    BinaryNodeRelation* rel = dynamic_cast<BinaryNodeRelation*>(o);
    if(rel) { viewer.relations.insert(std::tr1::shared_ptr<BinaryNodeRelation>(rel)); }
    objects.push_back(o);
  }
  std::cout << "[INFO]: read " << objects.size() << " elements" << std::endl;
  std::cout << "[INFO]: read " << viewer.nodes.size() << " local maps" << std::endl;
  
  viewer.init();
  viewer.show();
  while(viewer.isVisible()) {
    app.processEvents();
    if(viewer.needRedraw()) { viewer.updateGL(); }
    else { usleep(10000); }
  }

  return 0;
}
