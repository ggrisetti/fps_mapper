#include "trajectory_loop_closer_node_viewer.h"
#include "fps_gl_helpers/simple_viewer.h"
#include "fps_gl_helpers/simple_viewer.h"
#include "fps_gl_helpers/opengl_primitives.h"

using namespace boss;
using namespace std;
using namespace GLHelpers;

namespace fps_mapper {

  TrajectoryLoopCloserNodeViewer2::TrajectoryLoopCloserNodeViewer2(TrajectoryLoopCloser* trajectory_loop_closer_,
								  boss::IdContext* context_): LocalMapViewer(context_) {
    _save_screenshots = false;
    _save_map = false;
    _counter = 0;
    _id = 1;
    _last_tracker_transform = Eigen::Isometry3f::Identity();
    _last_node = 0;
    _last_relation = 0;
    _last_local_map = 0;
    _trajectory_loop_closer = trajectory_loop_closer_;
    _need_redraw = true;
  }

  void TrajectoryLoopCloserNodeViewer2::onNewLocalMap(LocalMap* lmap) {    
    LocalMapViewer::onNewLocalMap(lmap);
    _serializable_objects.push_back(lmap);
    _id++;
  }

  void TrajectoryLoopCloserNodeViewer2::onNewNode(MapNode* n) {
    LocalMapViewer::onNewNode(n);    
    _last_node = n;
    _serializable_objects.push_back(n);
    _id++;
  }

  void TrajectoryLoopCloserNodeViewer2::onNewRelation(BinaryNodeRelation* r) {
    LocalMapViewer::onNewRelation(r);    
    relations.insert(std::tr1::shared_ptr<BinaryNodeRelation>(r));
    _serializable_objects.push_back(r);
    LocalMap* to = dynamic_cast<LocalMap*>(r->to());     
    if(to) {       
      _last_local_map = to;      
      _last_tracker_transform = _last_local_map->transform();      
      _last_relation = r; 
      LocalMap* from = dynamic_cast<LocalMap*>(r->from());     
      if(from) {
	to->setTransform(from->transform() * r->transform());
      }
    }    
    _id++;
  }

  void TrajectoryLoopCloserNodeViewer2::onNewCameraInfo(BaseCameraInfo* cam) {
    _serializable_objects.push_back(cam);
    _id++;
  }

  void TrajectoryLoopCloserNodeViewer2::draw() {
    // draw current local map
    if(_trajectory_loop_closer->targetNode()) {
      int attrs = ATTRIBUTE_SHOW;
      attrs |= ATTRIBUTE_SELECTED;
      glPushMatrix();
      glColor3f(0.0f, 0.0f, 1.0f);
      _trajectory_loop_closer->targetNode()->draw(attrs);
      glPopMatrix();
    }
    
    // draw accepted closure nodes
    for(MapNodeSet::iterator it = _trajectory_loop_closer->acceptedClosureNodes().begin(); 
    	it != _trajectory_loop_closer->acceptedClosureNodes().end(); 
    	++it) {
      MapNode* n = *it;
      int attrs = ATTRIBUTE_SHOW;
      attrs |= ATTRIBUTE_SELECTED;
      glPushMatrix();
      glColor3f(0.0f, 1.0f, 0.0f);
      n->draw(attrs);
      glPopMatrix();
    }

    // draw candidate closure nodes
    for(MapNodeSet::iterator it = _trajectory_loop_closer->candidateClosureNodes().begin(); 
	it != _trajectory_loop_closer->candidateClosureNodes().end(); 
	++it) {
      MapNode* n = *it;
      int attrs = ATTRIBUTE_SHOW;
      attrs |= ATTRIBUTE_SELECTED;
      attrs |= ATTRIBUTE_ONLY;      
      glPushMatrix();
      glColor3f(1.0f, 0.0f, 0.0f);
      n->draw(attrs);
      glPopMatrix();
    }

    Eigen::Isometry3f T;
    Eigen::Isometry3f offset;
    // draw reference cloud
    if(_last_local_map) {
      offset = _last_tracker_transform.inverse() * _ref_pose;
      T = _last_local_map->transform() * offset;
    }
    else {
      T = _ref_pose;
    }
    glPushMatrix();
    glMultMatrix(T);
    glColor3f(0.5, 0.5, 0.5);
    _ref_cloud.draw();
    glPopMatrix();

    // draw current cloud
    if(_last_local_map) {
      offset = _last_tracker_transform.inverse() * _curr_pose;
      T = _last_local_map->transform() * offset;
    }
    else {
      T = _curr_pose;
    }    
    glPushMatrix();
    glMultMatrix(T);
    glColor3f(0.5, 0.5, 0.5);
    _curr_cloud.draw();
    glScalef(0.3, 0.3, 0.3);
    GLHelpers::drawReferenceSystem();
    glPopMatrix();

    // draw temporary nodes
    for(std::list<MapNode*>::iterator it = _temp_nodes.begin(); it != _temp_nodes.end(); it++) {
      if(_last_local_map) {
    	offset = _last_tracker_transform.inverse() * (*it)->transform();
      	T = _last_local_map->transform() * offset;
    	(*it)->push();
    	(*it)->setTransform(T);
    	(*it)->draw();
    	(*it)->pop();
      }
      else {
    	(*it)->draw();
      }
    }

    // draw nodes
    for(MapNodeList::iterator it = nodes.begin(); it != nodes.end(); it++) {
      MapNode* n = it->get();
      int attrs = ATTRIBUTE_SHOW;
      if(_selected_objects.count(n)) {
    	glColor3f(0.8, 0.5, 0.5);
    	attrs |=ATTRIBUTE_SELECTED;
      } 
      else {
    	glColor3f(0.5, 0.8, 0.5);
      }
      n->draw(attrs);
    }

    // draw relations
    for(BinaryNodeRelationSet::iterator it = relations.begin(); it != relations.end(); ++it) {
      LocalMap* from = dynamic_cast<LocalMap*>((*it)->from());
      LocalMap* to = dynamic_cast<LocalMap*>((*it)->to());      
      if(from && to) { 
	(*it)->draw(); 	
      }
    }

    // If enabled, save snapshot
    if(_save_screenshots) {
      char buffer[2048];
      sprintf(buffer, "snapshot_%05d.png", _counter);
      _counter++;
      this->setSnapshotFormat(QString("PNG"));
      this->setSnapshotQuality(10);
      this->saveSnapshot(QString(buffer), true);
    }

    _need_redraw = false;
  }

  void TrajectoryLoopCloserNodeViewer2::optimizeMap(int iterations) {
    _g2o_bridge.psToG2o(relations, nodes);
    _g2o_bridge.quietOptimize(iterations);
    _g2o_bridge.g2oToPs(nodes);
    _need_redraw = true;
  }

  void TrajectoryLoopCloserNodeViewer2::addClosuresAndOptimizeMap() {
    if(_trajectory_loop_closer->acceptedClosureRelations().size() > 0) {      
      relations.insert(_trajectory_loop_closer->acceptedClosureRelations().begin(), 
      		       _trajectory_loop_closer->acceptedClosureRelations().end());
      optimizeMap(25);
      publishUpdates();
    }
    LocalMap* local_map =  dynamic_cast<LocalMap*>(_trajectory_loop_closer->targetNode());
    _added_closures.insert(_trajectory_loop_closer->acceptedClosureRelations().begin(), 
			   _trajectory_loop_closer->acceptedClosureRelations().end());
    _need_redraw = true;
  }

  void TrajectoryLoopCloserNodeViewer2::publishUpdates() {
    MapUpdateMsg msg;
    msg.updates.resize(nodes.size());
    int k = 0;
    for(MapNodeList::iterator it = nodes.begin(); it != nodes.end(); ++it) {
      MapNode *n = it->get();
      msg.updates[k].node_id = n->getId();
      msg.updates[k].transform = eigen2pose(n->transform());
      k++;
    }
    _updates_pub.publish(msg);
  }

  void TrajectoryLoopCloserNodeViewer2::publishTf() { 
    if(_last_node == 0) {
      return;
    }

    Eigen::Isometry3f T;
    Eigen::Isometry3f offset;
    if(_last_local_map) {
      offset = _last_tracker_transform.inverse() * _curr_pose;
      T = _last_local_map->transform() * offset;
    }
    else {
      T = _curr_pose;
    }

    // Get the tracker transform
    tf::StampedTransform transform;
    try { 
      _tf_listener->waitForTransform("tracker_origin_frame_id", "/base_link",
				     ros::Time(_last_node->timestamp()), ros::Duration(0.05));
      _tf_listener->lookupTransform("tracker_origin_frame_id", "/base_link", 
				    ros::Time(_last_node->timestamp()), transform); 
    }
    catch(tf::TransformException ex) { 
      ROS_ERROR("%s", ex.what());
      return;
    }

    if(_tf_broadcaster) {
      // Publish correction transform from tracker frame
      Eigen::Isometry3f tracker_pose = tfTransform2eigen(transform);    
      Eigen::Isometry3f delta_pose = T * tracker_pose.inverse();
      _tf_broadcaster->sendTransform(tf::StampedTransform(eigen2tfTransform(delta_pose), 
							  ros::Time(_last_node->timestamp()), 
							  "loop_closer_origin_frame_id", 
							  "tracker_origin_frame_id"));
      
      Eigen::Matrix<float, 6, 1> global_pose_v = t2v(T);
      Eigen::Matrix<float, 6, 1> delta_pose_v = t2v(delta_pose);
      printf("\r[INFO]: global pose %f %f %f %f %f %f ===> delta pose %f %f %f %f %f %f",
	     global_pose_v[0], global_pose_v[1], global_pose_v[2],
	     global_pose_v[3], global_pose_v[4], global_pose_v[5],
	     delta_pose_v[0], delta_pose_v[1], delta_pose_v[2],
	     delta_pose_v[3], delta_pose_v[4], delta_pose_v[5]);
      fflush(stdout);
    }
  }  

  void TrajectoryLoopCloserNodeViewer2::init(ros::NodeHandle& n, tf::TransformListener* listener, tf::TransformBroadcaster* broadcaster) {
    _tf_broadcaster = broadcaster;
    LocalMapViewer::init(n, listener);
    _updates_pub = n.advertise<fps_mapper::MapUpdateMsg>("/global_optimizer/map_updates", 100);    
    setAxisIsDrawn(true);
    setBackgroundColor(QColor(255, 255, 255));
    _need_redraw = true;
  }

  void TrajectoryLoopCloserNodeViewer2::keyPressEvent(QKeyEvent *e) {
    if(e->key() == Qt::Key_C) { _selected_objects.clear(); }
    else if(e->key() == Qt::Key_V) { for(MapNodeList::iterator it = nodes.begin(); it!=nodes.end(); it++) { _selected_objects.insert(it->get()); } }
    else if(e->key() == Qt::Key_O) { optimizeMap(25); }
    else if(e->key() == Qt::Key_S) { _save_map = true; }
    else if(e->key() == Qt::Key_F) { _save_screenshots = !_save_screenshots; } 
    else { TrajectoryViewer::keyPressEvent(e); }
    _need_redraw = true;
  }

  void TrajectoryLoopCloserNodeViewer2::writeMap(std::string output_log) {
    std::cout << "[INFO]: saving log to " << output_log << std::endl;
    Serializer ser;
    ser.setFilePath(output_log);
    ser.setBinaryPath(output_log + ".d/<classname>.<nameAttribute>.<id>.<ext>");
    for(std::list<Serializable*>::iterator it = _serializable_objects.begin(); it != _serializable_objects.end(); ++it) {
      Serializable* s = *it;
      ser.writeObject(*s);
    }
    int id = _id;
    for(BinaryNodeRelationSet::iterator it = _added_closures.begin(); it != _added_closures.end(); ++it) {
      BinaryNodeRelation* closure = it->get();
      closure->setId(id);
      id++;
      ser.writeObject(*closure);
    }
    _save_map = false;
  }

}
